# README #

I would add notes regarding the application here, mostly regarding how to setup the app in a new server, design choices made etc.

# Bower

When a new angular library needs to be added, we use bower to install the library. Then, we put the path of the exact file in the library that we want, under the `assets` object in `config/env/all.js`. It will then automatically be added to front-end `app/views/layout.server.view.html` template.

# Angular App

The angular app is structured into 'modules'.

In the angular app, instead of putting all the controllers / routes / views together, we are splitting it up into section modules, i.e. the controller / route / view template of the homepage would reside together in a 'home' module. This makes it easy to understand and modify the code. We can copy an existing module's folder and start modifying its controller / route / template for every new page that needs to be implemented, thus optimising time.

# LESS

We prefer LESS files to CSS files for frontend-end styling. Every angular module can have a `less` folder which contains the LESS code for that specific module. The LESS file path then needs to be imported at the bottom of the `public/less/application.less` file. The grunt `less` task then auto-compiles the LESS files into `public/application.min.css`.

DO NOT make any updates in `public/application.min.css`. Those changes will be overwritten bu the 'grunt less' task.
