'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function(app) {
  // Admin Routes
  var admin = require('../../app/controllers/admin.server.controller');

  // Setting up the admin authentication api
  app.route('/admin/signin')
    .post(admin.signin);
    
  app.route('/admin/signout')
    .get(admin.signout);
};
