'use strict';

module.exports = function(app) {
	var admin = require('../../app/controllers/admin.server.controller');
	var editors = require('../../app/controllers/editors.server.controller');

	// Editors Routes
	app.route('/editors')
		.get(admin.requiresLogin, editors.list)
		.post(admin.requiresLogin, editors.create);

	app.route('/editors/:editorId')
		.get(admin.requiresLogin, editors.read)
		.put(admin.requiresLogin, editors.update)
		.delete(admin.requiresLogin, editors.delete);

	// Finish by binding the Editor middleware
	app.param('editorId', editors.editorByID);
};
