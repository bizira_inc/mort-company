'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Editor = mongoose.model('Editor'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, editor;

/**
 * Editor routes tests
 */
describe('Editor CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Editor
		user.save(function() {
			editor = {
				name: 'Editor Name'
			};

			done();
		});
	});

	it('should be able to save Editor instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Editor
				agent.post('/editors')
					.send(editor)
					.expect(200)
					.end(function(editorSaveErr, editorSaveRes) {
						// Handle Editor save error
						if (editorSaveErr) done(editorSaveErr);

						// Get a list of Editors
						agent.get('/editors')
							.end(function(editorsGetErr, editorsGetRes) {
								// Handle Editor save error
								if (editorsGetErr) done(editorsGetErr);

								// Get Editors list
								var editors = editorsGetRes.body;

								// Set assertions
								(editors[0].user._id).should.equal(userId);
								(editors[0].name).should.match('Editor Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Editor instance if not logged in', function(done) {
		agent.post('/editors')
			.send(editor)
			.expect(401)
			.end(function(editorSaveErr, editorSaveRes) {
				// Call the assertion callback
				done(editorSaveErr);
			});
	});

	it('should not be able to save Editor instance if no name is provided', function(done) {
		// Invalidate name field
		editor.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Editor
				agent.post('/editors')
					.send(editor)
					.expect(400)
					.end(function(editorSaveErr, editorSaveRes) {
						// Set message assertion
						(editorSaveRes.body.message).should.match('Please fill Editor name');
						
						// Handle Editor save error
						done(editorSaveErr);
					});
			});
	});

	it('should be able to update Editor instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Editor
				agent.post('/editors')
					.send(editor)
					.expect(200)
					.end(function(editorSaveErr, editorSaveRes) {
						// Handle Editor save error
						if (editorSaveErr) done(editorSaveErr);

						// Update Editor name
						editor.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Editor
						agent.put('/editors/' + editorSaveRes.body._id)
							.send(editor)
							.expect(200)
							.end(function(editorUpdateErr, editorUpdateRes) {
								// Handle Editor update error
								if (editorUpdateErr) done(editorUpdateErr);

								// Set assertions
								(editorUpdateRes.body._id).should.equal(editorSaveRes.body._id);
								(editorUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Editors if not signed in', function(done) {
		// Create new Editor model instance
		var editorObj = new Editor(editor);

		// Save the Editor
		editorObj.save(function() {
			// Request Editors
			request(app).get('/editors')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Editor if not signed in', function(done) {
		// Create new Editor model instance
		var editorObj = new Editor(editor);

		// Save the Editor
		editorObj.save(function() {
			request(app).get('/editors/' + editorObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', editor.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Editor instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Editor
				agent.post('/editors')
					.send(editor)
					.expect(200)
					.end(function(editorSaveErr, editorSaveRes) {
						// Handle Editor save error
						if (editorSaveErr) done(editorSaveErr);

						// Delete existing Editor
						agent.delete('/editors/' + editorSaveRes.body._id)
							.send(editor)
							.expect(200)
							.end(function(editorDeleteErr, editorDeleteRes) {
								// Handle Editor error error
								if (editorDeleteErr) done(editorDeleteErr);

								// Set assertions
								(editorDeleteRes.body._id).should.equal(editorSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Editor instance if not signed in', function(done) {
		// Set Editor user 
		editor.user = user;

		// Create new Editor model instance
		var editorObj = new Editor(editor);

		// Save the Editor
		editorObj.save(function() {
			// Try deleting Editor
			request(app).delete('/editors/' + editorObj._id)
			.expect(401)
			.end(function(editorDeleteErr, editorDeleteRes) {
				// Set message assertion
				(editorDeleteRes.body.message).should.match('User is not logged in');

				// Handle Editor error error
				done(editorDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Editor.remove().exec();
		done();
	});
});