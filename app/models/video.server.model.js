'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Video Schema
 */
var VideoSchema = new Schema({
  // Video model fields   
  created: {
    type: Date,
    default: Date.now
  },
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'Name cannot be blank'
  },
  description: {
    type: String,
    default: '',
    trim: true
  },
  artist: {
    type: String,
    trim: true,
    required: 'Artist not found'
  },
  vimeo_id: {
    type: Number,
    required: 'Vimeo Id cannot be blank'
  },
  vimeo_pic_id: {
    type: Number,
    required: 'Vimeo video picture id not found'
  }
});

mongoose.model('Video', VideoSchema);
