'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Editor = mongoose.model('Editor'),
	_ = require('lodash');

/**
 * Create a Editor
 */
exports.create = function(req, res) {
	var editor = new Editor(req.body);
	editor.user = req.user;

	editor.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(editor);
		}
	});
};

/**
 * Show the current Editor
 */
exports.read = function(req, res) {
	res.jsonp(req.editor);
};

/**
 * Update a Editor
 */
exports.update = function(req, res) {
	var editor = req.editor ;

	editor = _.extend(editor , req.body);

	editor.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(editor);
		}
	});
};

/**
 * Delete an Editor
 */
exports.delete = function(req, res) {
	var editor = req.editor ;

	editor.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(editor);
		}
	});
};

/**
 * List of Editors
 */
exports.list = function(req, res) { 
	Editor.find().sort('-created').populate('user', 'displayName').exec(function(err, editors) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(editors);
		}
	});
};

/**
 * Editor middleware
 */
exports.editorByID = function(req, res, next, id) { 
	Editor.findById(id).populate('user', 'displayName').exec(function(err, editor) {
		if (err) return next(err);
		if (! editor) return next(new Error('Failed to load Editor ' + id));
		req.editor = editor ;
		next();
	});
};

/**
 * Editor authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.editor.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
