/**
 * Bootstraps database for the development environment
 */


/**
 * Module dependencies
 */

var async = require('async');
var mongoose = require('mongoose');
var shell = require('shelljs');


/**
 * Dummy video data
 */

var dummyVideos = [

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 2',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 3',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 4',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 2',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 3',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 4',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 4',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 2',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 3',
    artist: 'Kilogramme',
  },

  {
    vimeo_id: '116746233',
    vimeo_pic_id: '503052780',
    name: 'Tall Tales 4',
    artist: 'Kilogramme',
  },

];


function bootstrapDevDB(cb) {


  async.series({


      loadApp: function(cb) {
        console.log('loading app ...');
        shell.exec('node ./server');
        cb();
      },

      createVideos: function(cb) {


        /**
         * Models
         */
        var Video = mongoose.model('Video');


        console.log('creating dummy videos ...');
        Video.create(dummyVideos, function(err) {
          if (err) {
            return cb(err);
          } else {
            cb();
          }
        })



      },



    },

    function(err) {

      if (err) {

        console.log('\n\nERROR:\n');
        console.log(err);

      } else {

        console.log('\n\n');
        console.log('Dummy data created successfully');

      }

      process.exit( !! err);
    });
}

bootstrapDevDB();
