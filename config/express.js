'use strict';

/**
 * Module dependencies.
 */
var bodyParser = require('body-parser');
var compression = require('compression');
var consolidate = require('consolidate');
var cookieParser = require('cookie-parser');
var express = require('express');
var favicon = require('static-favicon');
var flash = require('connect-flash');
var helpers = require('view-helpers');
var methodOverride = require('method-override');
var morgan = require('morgan');
var passport = require('passport');
var path = require('path');
var serveStatic = require('serve-static');
var session = require('express-session');

var config = require('./config');

var MongoStore = require('connect-mongo')({
  session: session
});


module.exports = function(db) {

  var app = express();

  // Globbing model files
  config.getGlobbedFiles('./app/models/**/*.js')
    .forEach(function(modelPath) {
      require(path.resolve(modelPath));
    });

  // Setting application local variables
  app.locals.title = config.app.title;
  app.locals.description = config.app.description;
  app.locals.keywords = config.app.keywords;
  app.locals.jsFiles = config.getJavaScriptAssets();
  app.locals.cssFiles = config.getCSSAssets();

  // Passing the request url to environment locals
  app.use(function(req, res, next) {
    res.locals.url = req.protocol + '://' + req.headers.host + req.url;
    next();
  });

  // Prettify HTML
  app.locals.pretty = true;


  //Should be placed before express
  app.use(compression({
    filter: function(req, res) {
      return (/json|text|javascript|css/)
        .test(res.getHeader('Content-Type'));
    },
    level: 9
  }));

  app.set('showStackError', true);

  //Don't use logger for test env
  if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));

    // disable views cache in development
    app.set('view cache', false);

  } else if (process.env.NODE_ENV === 'production') {

    // cache=memory or swig dies in NODE_ENV=production
    app.locals.cache = 'memory';
  }

  // assign the template engine to server.view.html files
  app.engine('server.view.html', consolidate[config.templateEngine]);

  // set server.view.html as the default extension
  app.set('view engine', 'server.view.html');

  // Set views path, template engine and default layout
  app.set('views', config.root + '/app/views');

  // Enable jsonp
  app.enable('jsonp callback');

  // Request body parsing middleware should be above methodOverride
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride());

  // The cookieParser should be above session
  app.use(cookieParser());

  // Express/Mongo session storage
  app.use(session({
    secret: config.sessionSecret,
    store: new MongoStore({
      url: config.db,
      collection: config.sessionCollection
    })
  }));

  // use passport session
  app.use(passport.initialize());
  app.use(passport.session());

  // Dynamic helpers
  app.use(helpers(config.app.name));

  // Connect flash for flash messages
  app.use(flash());

  // Setting the fav icon and static folder
  app.use(favicon(config.root + '/public/favicon.ico'));
  app.use(serveStatic(config.root + '/public'));


  // Globbing routing files
  config.getGlobbedFiles('./app/routes/**/*.js')
    .forEach(function(routePath) {
      require(path.resolve(routePath))(app);
    });


  // Assume "not found" in the error msgs is a 404. this is somewhat
  // silly, but valid, you can do whatever you like, set properties,
  // use instanceof etc.
  app.use(function(err, req, res, next) {
    // Treat as 404
    if (~err.message.indexOf('not found')) return next();

    // Log it
    console.error(err.stack);

    // Error page
    res.status(500)
      .render('500', {
        error: err.stack
      });
  });


  // Assume 404 since no middleware responded
  app.use(function(req, res) {
    res.status(404)
      .render('404', {
        url: req.originalUrl,
        error: 'Not Found'
      });
  });

  // return the server instance
  return app;
};
