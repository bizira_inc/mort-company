'use strict';

var path = require('path'),
  rootPath = path.normalize(__dirname + '/../..');

module.exports = {

  app: {
    title: 'The Mort Company',
    description: 'The Mort Company - Tell A Vision',
    keywords: 'Mort Company, Videos'
  },


  root: rootPath,
  port: process.env.PORT || 3000,
  db: process.env.MONGOHQ_URL,

  // Template Engine
  templateEngine: 'swig',

  // The secret should be set to a non-guessable string that
  // is used to compute a session hash
  sessionSecret: 'company-mort-the',
  // The name of the MongoDB collection to store sessions in
  sessionCollection: 'sessions',


  assets: {
    lib: {
      css: [
      ],
      js: [
        'public/lib/jquery/jquery.js',
        // 'public/lib/jquery-bridget/jquery.bridget.js',
        // 'public/lib/imagesloaded/imagesloaded.pkgd.js',
        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-cookies/angular-cookies.js',
        // 'public/lib/angular-animate/angular-animate.js',
        // 'public/lib/angular-touch/angular-touch.js',
        // 'public/lib/angular-sanitize/angular-sanitize.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.js',
        // 'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
        // 'public/lib/masonry/dist/masonry.pkgd.js',
        // 'public/lib/angular-masonry/angular-masonry.js',
        // 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.0.0/isotope.pkgd.js',
        // 'public/lib/angular-isotope/dist/angular-isotope.js',

      ]
    },
    css: [
      'public/application.min.css',
      'public/modules/**/css/*.css'
    ],
    js: [
      'public/config.js',
      'public/application.js',
      'public/modules/*/*.js',
      'public/modules/*/*[!tests]*/*.js'
    ],
    tests: [
      'public/lib/angular-mocks/angular-mocks.js',
      'public/modules/*/tests/*.js'
    ]
  }
}
